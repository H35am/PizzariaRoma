﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PizzariaRoma.Startup))]
namespace PizzariaRoma
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
