﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PizzariaRoma.Models
{
    public class Ingredient
    {
        public int Id { get; set; }

        [Required, MaxLength(45)]
        public string Name { get; set; }
        [Required, MaxLength(250)]
        public string Description { get; set; }
        
    }
}