﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Profile;

namespace PizzariaRoma.Models
{
    public class Pizza
    {
        public int Id { get; set; }

        [Required, MaxLength(45)]
        public string Name { get; set; }
        [Required, MaxLength(250)]
        public string Description { get; set; }
        public decimal Price { get; set; }
        [Column(TypeName = "image")]
        public byte[] Image { get; set; }

        [Required]
        public Ingredient Ingredient { get; set; }
        public int IngredientId { get; set; }   

    }
}