﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PizzariaRoma.Models;

namespace PizzariaRoma.Controllers
{
    public class PizzaController : Controller
    {
        private ApplicationDbContext _context;

        public PizzaController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Pizza
        public ActionResult Index()
        {
            var pizzas = _context.Pizzas.Include(p => p.Id).ToList();

            if (pizzas == null)
                return HttpNotFound();

            return View(pizzas);
        }

        public ActionResult Detail(int id)
        {

            var pizza = _context.Pizzas.Include(p => p.Id).FirstOrDefault(m => m.Id == id);

            if (pizza == null)
                return HttpNotFound();

            return View(pizza);

        }

    }
}