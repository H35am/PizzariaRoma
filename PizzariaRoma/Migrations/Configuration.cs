﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using PizzariaRoma.Models;

namespace PizzariaRoma.Migration
{
    public class Configuration : DbMigrationsConfiguration<PizzariaRoma.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PizzariaRoma.Models.ApplicationDbContext context)
        {
            base.Seed(context);
        }
    }
}